/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sample.jetty.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.servlet.ModelAndView;
import sample.jetty.model.Message;
import sample.jetty.model.MessageRepository;
import sample.jetty.service.HelloWorldService;

@Controller
@RequestMapping("/")
public class SampleController {

	@Autowired
	private HelloWorldService helloWorldService;


    private final MessageRepository messageRepository;

    @Autowired
    public SampleController(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @RequestMapping
    public ModelAndView list() {
        Iterable<Message> messages = this.messageRepository.findAll();
        return new ModelAndView("message/list","messages",messages);
    }

//	@RequestMapping
//	public ModelAndView wellcome() {
//
//        this.helloWorldService.getHelloMessage();
//        String[] ar = {"message1","message2"};
//
//        return new ModelAndView("message/list","messages",ar);
//	}

}
