package sample.jetty.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

/**
 * Created by kudenv on 3/5/15.
 */

@Component
@org.springframework.context.annotation.Configuration
public class Configuration {

//    @Bean
//    public ServletContextTemplateResolver servletContextTemplateResolver(){
//        ServletContextTemplateResolver resolver = new ServletContextTemplateResolver();
//        resolver.setPrefix("/resources/templates");
//        resolver.setSuffix(".html");
//        resolver.setTemplateMode("HTML5");
//        return resolver;
//    }
//
//    @Bean
//    public SpringTemplateEngine springTemplateEngine(){
//        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
//        templateEngine.setTemplateResolver(servletContextTemplateResolver());
//        return  templateEngine;
//    }
//
//
//    @Bean
//    public ThymeleafViewResolver thymeleafViewResolver() {
//        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
//        resolver.setOrder(1);
//        resolver.setTemplateEngine(springTemplateEngine());
//
//        return resolver;
//    }

    @Bean
    public InternalResourceViewResolver internalResourceViewResolver(){
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/resources/jsps/");
        resolver.setSuffix(".jsp");
        resolver.setOrder(2);
        resolver.setViewNames("jsp");
        return resolver;
    }



}
